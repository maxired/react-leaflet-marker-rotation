import React, { Component } from 'react';
import logo from './logo.svg';

import './App.css';

import MarkerLayer from 'react-leaflet-marker-layer';

import { Map, TileLayer } from 'react-leaflet';

class ExampleMarkerComponent extends React.Component {

  render() {

    const { rot = '0'} = this.props.marker;
    const style = {
      border: 'solid 1px lightblue',
      backgroundColor: '#333333',
      marginTop: '-12px',
      marginLeft: '-12px',
      width: '70px',
      height: '24px',
      transform: `rotate(${rot}deg)`
    };

    return (
      <div>
        <div style={Object.assign({}, this.props.style, style)}></div>
      </div>
    );
  }

}


class App extends Component {

  refMarker(ref){
    ref.props.map.on('zoom', () => {
      ref.updatePosition()
    })
  }

  render() {
    const position = { lng: -122.673447, lat: 45.522558 };
const markers = [
  {
    position: { lng: -122.67344700000, lat: 45.522558100000 },
    text: 'Voodoo Doughnut',
    rot: '25'
  },
  {
    position: { lng: -122.67814460000, lat: 45.5225512000000 },
    text: 'Bailey\'s Taproom',
    rot: '65'
  },
  {
    position: { lng: -122.67535700000002, lat: 45.5192743000000 },
    text: 'Barista'
  },
  {
    position: { lng: -122.65596570000001, lat: 45.5199148000001 },
    text: 'Base Camp Brewing'
  }
];

    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to
        </p>
          <Map center={position} zoom={13}>
            <MarkerLayer
              markers={markers}
              longitudeExtractor={m => m.position.lng}
              latitudeExtractor={m => m.position.lat}
              markerComponent={ExampleMarkerComponent}
              ref={this.refMarker}
            />
            <TileLayer
              url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
              attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              />
          </Map>
      </div>
    );
  }
}

export default App;
